import sqlite3 as sql
import datetime


######## chat.db  ---> Msgid, Message, Senderid, Upvotes, Downvotes, Timestamp


def getsenderid(tablename,msgid):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	sid = cp.execute("SELECT Senderid From (?) Where Msgid = (?)",(tablename,msgid)).fetchall()
	cptr.commit()
	cptr.close()
	if sid == []:
		ret = False
	else:
		return sid[0][0]


def upvote(tablename,msgid):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	data = cp.execute("SELECT Upvotes,Senderid From (?) where Msgid =(?)",(tablename,msgid)).fetchall()
	upvotes = data[0][0]
	senderid = data[0][1]
	cp.execute("UPDATE (?) Set Upvotes = (?) where Msgid = (?) ",(tablename,upvotes+1,msgid))
	cptr.commit()
	cptr.close()
	s_upvote(senderid)


def downvote(tablename,msgid):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	data = cp.execute("SELECT Downvotes,Senderid From (?) where Msgid =(?)",(tablename,msgid)).fetchall()
	downvotes = data[0][0]
	senderid = data[0][1]
	cp.execute("UPDATE (?) Set Downvotes = (?) where Msgid = (?) ",(tablename,downvotes+1,msgid))
	cptr.commit()
	cptr.close()
	s_downvote(senderid)



def tabexists(tablename):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	st = True
	try:
		ret = cp.execute("SELECT * from TabData where Tablename = (?)",(tablename, )).fetchall()
	except Exception as e:
		print(e)
		st = False
	finally:
		cptr.commit()
		cptr.close()		
	if not st :#or ret == []:
		return False
	if ret == []:
		print("EMPTY!!!")
		return False
	return True


def tabcreate(ttype,tablename):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	st = True
	try:
		cp.execute("create table (?) (Msgid Integer primary key auto increment,Message Text,Senderid integer not null,Upvotes integer not null,Downvotes integer not null,Timestamp datetime); ")
		cp.execute("INSERT into TabData (Tablename,Tabletype) values (?,?)",(tablename,ttype))
	except:
		st = False
	finally:
		cptr.commit()
		cptr.close()
	return st


def drop(tablename):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	cp.execute("DROP table if exists (?)")
	cp.execute("DELETE from TabData Where Tablename=(?)",(tablename,))
	cptr.commit()
	cptr.close()


def insert(tablename,message,senderid):
	cptr = sql.connect("chat.db")
	cp= cptr.cursor()
	cp.execute("INSERT into (?) (Message,Senderid,Upvotes,Downvotes) values (?)",(tablename,message,senderid,0,0))
	cptr.commit()
	cptr.close()


def load(ttype):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	ret = cp.execute("SELECT Tablename from TabData Where Tabletype = (?)",(ttype,)).fetchall()
	cptr.commit()
	cptr.close()
	return ret


def chatload(tablename):
	cptr = sql.connect("chat.db")
	cp = cptr.cursor()
	#chat = cp.execute("SELECT * from (SELECT Tablename from TabData where Tablename = (?)) ;",(tablename,)).fetchall()
	chat = cp.execute("SELECT * from MathsChat ").fetchall()
	
	cptr.commit()
	cptr.close()
	return chat