from flask import *
from functionfile import *

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def init():
    print("render init.html")
    return render_template('init.html',template_folder = "templates")


@app.route('/home', methods=['GET', 'POST'])
def home():
    return render_template('home.html', template_folder="templates")


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == "POST":
        print("login request POST")

        #email = request.form['email']
        #email = request.form['email']
        username = request.form['email']
        #pin = request.form['password']
        hpass = request.form['password']
        #print(email, password)
        #hashed_pass = hasher(password)
        #state = verify(email, hashed_pass)
        if uexists(username) :
            success = verify(username,hpass)
            if not success:
                error = 402
            print(success)
        #print(state)
            if success == True:
                return redirect(url_for('home'))
            elif success == False:
                return redirect(url_for('login'))
    return render_template('login.html', error=error, template_folder="templates")


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':

        email = request.form['email']
        username = request.form['email']
        pin = request.form['password']
        hpass = request.form['password']

        #hashed_pass = hasher(password)
        #state = user_verify(username)

        if uexists(username):
            return "<h1>Username already exists. Please try another Username.</h1><br/><a href= '/signup'>Sign Up Page</a>"

        if eexists(email):
            return "<h1>E-mail Id already exists. Please recheck you E-mail Id.</h1><br/><a href= '/signup'>Sign Up Page</a>"

        adduser(username,username,hpass,hpass,email)

        '''if state == 'True':
            return redirect(url_for('login'))
        elif state == 'False':
            return redirect(url_for('home'))
        '''
    return render_template('signup.html', template_folder="templates")

if __name__ == '__main__':
    app.secret_key = 'jjbgrsgwoghsoo'
    app.run(debug=True)
