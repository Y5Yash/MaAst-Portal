from flask import Flask, sessions, render_template, redirect, url_for, request, flash
from functionfile import *
from myfuncs import *

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def init():
    print("render init.html")
    return render_template('gravity.html',template_folder = "templates")


@app.route('/home', methods=['GET', 'POST'])
def home():
    return render_template('home.html', template_folder="templates")

@app.route('/gravity', methods=['GET', 'POST'])
def gravity():
    return render_template('gravity.html', template_folder="templates")


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == "POST":
        print("login request POST")

        #email = request.form['email']
       # email = request.form['email']
        username = request.form['username']
       # pin = request.form['password2']
        pin = request.form['password']
        print(username, pin)
        #print(email, password)
        #hashed_pass = hasher(password)
        #state = verify(email, hashed_pass)
        hpin = pinit(pin)
        if uexists(username) :
            success = authenticate(username,hpin)
            if not success:
                error = 406
            #else:

        #print(state)
        if success == True:
            return redirect(url_for('gravity'))
        elif success == False:
            return redirect(url_for('login'))
    return render_template('login.html', error=error, template_folder="templates")


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':

        name = request.form['name']
        email = request.form['email']
        username = request.form['username']
        pin = request.form['password2']
        password = request.form['password']
        hpass = hashit(password)
        hpin = pinit(pin)
        #hashed_pass = hasher(password)
        #state = user_verify(username)

        if uexists(username):
            return "<h1>Username already exists. Please try another Username.</h1><br/><a href= '/signup'>Sign Up Page</a>"
        
        if eexists(email):
            return "<h1>E-mail Id already exists. Please recheck you E-mail Id.</h1><br/><a href= '/signup'>Sign Up Page</a>"
        
        if (password != pin):
            return "<h1>Passwords Do Not Match!!!. Please recheck your Passwords.</h1><br/><a href = '/signup'>Sign Up Page</a>"

        adduser(username,name,hpass,hpin,email)

        #if state == 'True':
         #   return redirect(url_for('login'))
        #elif state == 'False':
         #   return redirect(url_for('home'))
        
    return render_template('login.html', template_folder="templates")








if __name__ == '__main__':
    app.run(debug=True)
