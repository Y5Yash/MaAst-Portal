drop table if exists TabData;
create table TabData(
		Tableid Integer primary key autoincrement,
		Tablename Text not null unique,
		Tabletype Integer not null
		);


drop table if exists MathsChat;
create table MathsChat
(
		Msgid Integer primary key autoincrement,
		Message Text,
		Senderid integer not null,
		Upvotes integer not null,
		Downvotes integer not null,
		Timestamp datetime 
);
