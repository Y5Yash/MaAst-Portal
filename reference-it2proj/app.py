#!/usr/bin/env python3

from flask import Flask, render_template, redirect, url_for, request

from mail import *
from login import *

app = Flask(__name__)

coder=""
usrn=""
global shot 
global usu  

@app.route('/')
def home():
    return render_template('front.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        
        usrn = request.form['email']
        pwd = request.form['password']
        
        hash_pass=hasher(pwd)
        state = verify(usrn, hash_pass)
        
        if state == False:
            print("Wrong Credentials")
            return redirect(url_for('login'))
        elif state == True:
            return redirect(url_for('home'))
    return render_template('login.html', error=error)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    error = None
    if request.method == 'POST':
        
        usrn = request.form['email']
        pwd = request.form['password']
        unam = request.form['name']
        
        hash_pass = hasher(pwd)
        state = user_verify(usrn)
        
        if state == True:
            print("User already present!")
            return redirect(url_for('login'))
        elif state == False:
            useradd(usrn, hash_pass,unam)
            print("User Added")
            return redirect(url_for('home'))

    return render_template('register.html', error=error)


@app.route('/forgot', methods=['GET', 'POST'])
def forgot():
    error = None
    if request.method == 'POST':
        usrn = request.form['email']
        state = user_verify(usrn)
        if(state==True):
            coder=mailer(usrn)
            print(coder)
            codeadd(coder)
            usradd(usrn)
            return redirect(url_for('reset'))
        else:
            print("Wrong Username")
            return redirect(url_for('forgot'))
            

    return render_template('forgot.html', error=error)


@app.route('/reset', methods=['GET', 'POST'])
def reset():
    error = None
    if request.method == 'POST':
        
        pwd = request.form['password']
        code = request.form['name']
        coder=codetake()
        usrn=usertake()
        print(coder)
        print(usrn)
        pwdn=hasher(pwd)
        if(coder==code):
            changepwd(usrn,pwdn)
            return redirect(url_for('login'))

        else:
            print("Wrong code")
            return redirect(url_for('forgot'))



    return render_template('reset.html', error=error)


if __name__ == '__main__':
    app.run(debug=True)
