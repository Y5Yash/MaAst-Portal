import sqlite3 as sql
#import hashlib
import passlib.hash

def uexists(username):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute("SELECT Username FROM UserProfile WHERE Username = (?)",(username,)).fetchall()
	upptr.close()
	if (usr==[]):
		return False
	else:
		return True


def eexists(email):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute("SELECT Email FROM UserProfile WHERE Email = (?)",(email,)).fetchall()
	upptr.close()
	if (usr==[]):
		return False
	else:
		return (usr[0])[0]


def adduser(username,name,hpass,email):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	up.execute(
		"INSERT into UserProfile ( Username, Name, Password , Email, Upvotes, Downvotes ) Values(?,?,?,?,?,?)",
		(username,name,hpass,email,0,0))
	upptr.commit()
	upptr.close()


'''
def authenticate(username, hpin):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute("SELECT Username FROM UserProfile WHERE Username = (?)",(username,)).fetchall()
	
	if (usr == []):
		return False
	else:
		uppin = up.execute("SELECT Checktoken FROM UserProfile where Username=(?)",(username,)).fetchall()
		upptr.commit()
		upptr.close()
		uphpin = (uppin[0])[0]
		return passlib.hash.pbkdf2_sha256.verify(hpin,uphpin)
'''



def exists(username):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute()

	upptr.commit()
	upptr.close()


def verify(username, password):
    upptr = sql.connect("profile.db")
    up = upptr.cursor()
    stx = up.execute("SELECT Password FROM UserProfile WHERE Username =(?)", (username,)).fetchall()
    sty = up.execute("SELECT Username FROM UserProfile WHERE Username =(?)", (username,)).fetchall()
    upptr.commit()
    upptr.close()

    if(sty == []):
        return False
    else:
        #pwx = "".join(stx[0])
        correct = (stx[0])[0]
        return (passlib.hash.pbkdf2_sha256.verify(password,correct))

def getuser(username):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	data = up.execute("SELECT * from UserProfile WHERE Username = (?)",(username,)).fetchall()
	upptr.commit()
	upptr.close()

	return data[0]

def s_upvote(userid):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	upvotes=up.execute("SELECT Upvotes from UserProfile where Userid = (?)",(userid,)).fetchall()
	upvotes = upvotes[0][0]
	up.execute("UPDATE UserProfile set Upvotes = (?) Where Userid = (?)",(upvotes + 1, userid))
	upptr.commit()
	upptr.close()



def s_downvote(userid):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	downvotes=up.execute("SELECT Downvotes from UserProfile where Userid = (?)",(userid,)).fetchall()
	downvotes = downvotes[0][0]
	up.execute("UPDATE UserProfile set Downvotes = (?) Where Userid = (?)",(Downvotes + 1, userid))
	upptr.commit()
	upptr.close()


#eexists(yash)