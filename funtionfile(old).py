import sqlite3 as sql
#import hashlib
import passlib.hash

def uexists(username):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute("SELECT Username FROM UserProfile WHERE Username = (?)",(username,)).fetchall()
	upptr.close()
	if (usr==[]):
		return False
	else:
		return True


def eexists(email):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute("SELECT Email FROM UserProfile WHERE Email = (?)",(email,)).fetchall()
	upptr.close()
	if (usr==[]):
		return False
	else:
		return (usr[0])[0]


def adduser(username,name,hpass,hpin,email):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	up.execute(
		"INSERT into UserProfile ( Username, Name, Password , Checktoken, Email ) Values(?,?,?,?,?)",
		(username,name,hpin,hpin,email))
	upptr.commit()
	upptr.close()



def authenticate(username, hpin):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute("SELECT Username FROM UserProfile WHERE Username = (?)",(username,)).fetchall()
	
	if (usr == []):
		return False
	else:
		uppin = up.execute("SELECT Checktoken FROM UserProfile where Username=(?)",(username,)).fetchall()
		upptr.commit()
		upptr.close()
		uphpin = (uppin[0])[0]
		return passlib.hash.pbkdf2_sha256.verify(hpin,uphpin)
	



def exists(username):
	upptr = sql.connect("profile.db")
	up = upptr.cursor()
	usr = up.execute()

	upptr.commit()
	upptr.close()


def verify(username, hpass):
    upptr = sql.connect("profile.db")
    up = upptr.cursor()
    stx = up.execute("SELECT Password FROM UserProfile WHERE Username =(?)", (username,)).fetchall()
    sty = up.execute("SELECT Username FROM UserProfile WHERE Username =(?)", (username,)).fetchall()
    upptr.commit()
    upptr.close()

    if(sty == []):
        return False
    else:
        #pwx = "".join(stx[0])
        pwx = (stx[0])[0]
        if (pwx == hpass):
            return True
        else:
            return False


#eexists(yash)